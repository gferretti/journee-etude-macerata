---
title: Partecipa online
heroImage: /images/habitat-futuriste.jpg
---

### Link per la videoconferenza

[**https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join**](https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join)

Questo link è soggetto a modifiche fino al giorno del convegno. Si prega di tornare su questa pagina e aggiornarla martedì 9 aprile 2024. 

L'evento si terrà martedì 9 aprile, dalle 9:00 alle 12:20 e dalle 14:00 alle 17:30 (UTC).


