---
title: "Human Beings and Machines:"
subtitle: "Stabilizing and Destabilizing Boundaries"
content: "April 9<sup>th</sup>, 2024"
heroImage: /images/habitat-futuriste.jpg

# blocs:
# - type: prose
#   Content: |
#     Bla bla bla
---


{{< blocs/prose id="introduction" >}}

## A one-day conference on the relationships between humans and machines

What is a machine? What is a human being? Is there a clear, stable boundary between the two? The many discussions about AI have recently renewed interest in these questions, which have haunted us for centuries. It would seem that in our relationship with machines, in the way we define and understand them, and above all in the way we identify how they are different from us, our very essence is at stake. As human beings, we define ourselves in opposition to machines.
This symposium aims to question this demarcation line by challenging the oppositions between human and machine, artificial and natural, quantifiable and non-quantifiable, calculable and non-calculable.

{{< /blocs/prose>}}


{{< blocs/texte-exergue >}}
Università di Macerata/Université de Montréal
{{< /blocs/texte-exergue >}}


{{< blocs/horaire entete="Schedule" id="schedule" >}}
{{< blocs/item-horaire
    heure="09:00"
    titre="Welcome speech"
    personnes="John Mc Court, Dean of the University of Macerata"
>}}
  
{{< blocs/item-horaire
    heure="9:15"
    titre="Introduction: The Day After... John McCarthy"
    personnes="Carla Canullo"
    resume=" "
  >}}

{{< blocs/item-horaire
    heure="9:30"
    titre="Access to Cultural Resources in the Age of AI: Concepts and Challenges"
    personnes="Pierluigi Feliciati"
    resume="What do we actually know about the experience of access and use of cultural resources published on the Internet? Based on the study coordinated within the InterPARES Trust AI project, the speaker will present and discuss the main concepts and challenges related to Reference and Access. The focus will be on resource quality, adopting a user-centred approach in the conception and development of resources, highlighting the utility of user studies to conceive, build and maintain quality digital services. Then, a draft of the conceptual model of access/use will be presented and discussed, extracting the challenges opened by the role AI agents as users and re/users."
>}}

{{< blocs/item-horaire
    heure="10:10"
    titre="Bridging the Semantic Gap Between Humans and Machine Vision"
    heure="10:10"
    titre="Bridging the Semantic Gap Between Humans and Machine Vision"
    personnes="Tiberio Uricchio"
    resume=""
>}}

{{< blocs/item-horaire
    heure="10:50"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="11:00"
    titre="Human-AI Teaming in the Age of Intelligent Systems"
    personnes="Emanuele Frontoni"
    resume="This talk explores the concept of Human-AI Teaming, emphasizing its transformative impact across digital humanities, health and welfare, and sustainability. In digital humanities, AI's advanced data analysis capabilities have revolutionized historical research, enhancing accessibility to cultural insights while challenging traditional methodologies. In health and welfare, AI aids in precise disease diagnostics and mental health interventions, improving patient outcomes and destabilizing conventional healthcare models  through personalized medicine. Sustainability efforts benefit from AI through improved environmental monitoring and energy management, promoting more efficient resource use and challenging existing environmental practices. The talk underscores the dual ability of Human-AI Teaming to stabilize and destabilize sectors, advocating for ethical, human-centric designs to ensure these integrations contribute positively to society."
>}}

{{< blocs/item-horaire
    heure="11:40"
    titre="Being Online and Offline: Utopia of Natural Presence"
    personnes="Peppe Cavallari"
    resume="Presence is always the presence of someone, or something, for someone else. Phenomenological analysis and media archeology show that all presence is embodied and materialized as the effect of a relationship, a convention and a technical protocol, three components of the interface. The daily experience of the communication technology of digital society, allows us to expose a major misunderstanding : to consider physical presence as a form of ideal presence, the only real presence as opposed to the remote presence mediated by screens, writing and imagery. Opposing the myth of natural and immediate presence, we will highlight the inextricably mediated and performative structure of presence, hypothesizing that in the age of permanent connection, presence opens up and is created in the gap and spatio-temporal interplay between being online and offline, two phases of our being-in-the-world."
>}}

{{< blocs/item-horaire
    heure="12:20"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="14:00"
    titre="Secretary and Boss agree: Reflection on the Good Literary Machines"
    personnes="Margot Mellet"
    resume="Secretary to conversational agent, the approach to writing as a mechanizable service introduces hybrid figures of women that are not limited to a past age. Whether in the imaginary form of literature, a cultural phenomenon or technical realities, the woman is the figure that serves man’s word: the petites mains that transcribes men’s thoughts as he dictates, or the speech intelligence programmed to reply or obey their requests. The progressive mechanization of modern writing with the development of typewriters has produced a general movement of displacement: man’s position in the production chain changes, and the gender of writing is reversed (Kittler, Gramophone, Film, Typewriter 1986), giving women a possible career in the publishing chain. This doorway into the world of literature, where ideas of the world emerge, may represent an emancipation and a professional path in many aspects, but it is also a double-edged sword. In the pairing of secretary and boss/author, the partnership is a matter of service, of word processing, but also of the domination of one state of being over another. In this case, the oscillation between woman in service and machine in order sets the boundaries of an essentially masculine human norm. Up to the first conversational agents, which are largely inspired by a misogynistic, heteronormative approach to service, the machine conveys an imaginary of a masculine being, a being that engenders a highly transhumanist ideal of the feminine. By analyzing literary and media moments - the writing spouses of several authors, including Nietzsche, the cultural evolution of the typewriter, the figure of the andreide in Villiers de L’Isle-Adam’s Eve of the Future, the misogynistic reactions of the first conversational agents - the presentation will discuss the blur between machine and woman that is the basis of an entire literary culture, but also of a media culture."
>}}

{{< blocs/item-horaire
    heure="14:40"
    titre="From Data Structure to World Structure"
    personnes="Giulia Ferretti"
    resume="Data structures organise knowledge into distinct categories and make it accessible online, contributing to defining reality. Ideally devoid of ambiguity, data architectures have semantic value, so that they embody information crucial to the construction of our opinions and knowledge. How ca we bring to light the influence of data structures on the meaning of the objects they represent? How can we encourage a collective reflection on their influence on the reality around us? Through the interrogation of various Application Programming Interfaces (APIs), I will show how technical infrastructures challenge and guide us to redefine the objects described through such technologies and, at the same time, I will investigate the preconceptions and human interests that drive the examined data structures. Such an inquiry will prove necessary both to obviate the risk of resigning ourselves to the incomprehensibility of data and of their influence and to counter any belief that the structures organising such data are merely neutral containers of purely human ideas and behaviours. Building from the theoretical conclusions of the disciplinary field of critical code studies, my aim is to demonstrate that data structuring is a process ontologically composed of discursive and cultural practices and, at the same time, of material activities that decompose information, abstract it, and thus conceive and conceptualize it. In this composite set of interests and forces, machinic and cultural influences redefine each other and participate in the signification of structured data, determining their influence on our reality."
>}}

{{< blocs/item-horaire
    heure="15:20"
    titre="The Inhuman Chrysalis"
    personnes="Ollivier Dyens"
    resume="What does it mean to be human in a world so strange that it feels alien, in-human, non-human? What does it mean to be human in a world where machines create awe inspiring artwork, where machines are more empathetic than we are, where machines alter the way we think, love and see beauty? What does it mean to be human in a world where technology is a cocoon from which we emerge as a chrysalis, half-human, half-“alien”? What will be our new ontology?"
>}}

{{< blocs/item-horaire
    heure="16:00"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="16:10"
    titre="Human-Machine Interaction and Love: The Next Generation of Dating Sites"
    personnes="Michael Sinatra"
    resume="Dating sites always prided themselves on using mathematically-driven methods to match people. As the rhetoric of using “unique” algorithms to improve matches continue to get more prominent in advertisements, it was followed in the last couple of years with more automated, A.I driven prompts. But now full exchanges are done by A.I assisted tool such as chatGPT making the idea of meeting someone “real” even more complex, however much it is the point of these sites in the first place."
>}}

{{< blocs/item-horaire
    heure="16:50"
    titre="The Factory of Subalterns: Humans, Machines and Subordinations"
    personnes="Marcello Vitali-Rosati"
    resume="Since the launch of ChatGPT, the public arena has been abuzz with talk of AI, the human/machine relationship and a variety of other anxieties. This interest reveals our unbridled fear that ”human”’s place in the world is under threat. What anguishes us about LLMs is the fact that they seem to undermine discursive strategies that enable the creation of hierarchical devices in which a separation between the dominant and the dominated is clearly defined. The hypothesis I’m going to present in my talk is as follows: discursive strategies for defining the human in opposition to something else - such as animals, automatons or machines - are in fact, always and above all, strategies for producing social, cultural and, more generically, symbolic hierarchies that can be understood as a veritable factory of subalterns."
>}}
{{< /blocs/horaire >}}


{{< blocs/partenaires entete="Our Partners" id="partners" >}}

{{< blocs/item-partenaires
    src="/images/partenaires/unimc-logo-transparent.png"
    alt="Università di Macerata"
>}}


{{< blocs/item-partenaires
    src="https://horizons.ecrituresnumeriques.ca/images/Logo/UdeM.png"
    alt="Université de Montréal"
>}}

{{< blocs/item-partenaires
    src="/images/logo-fr.svg"
    alt="Chaire de recherche du Canada sur les écritures numériques"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/doppio-diploma-v1.png"
    alt="Doppio diploma in Scienze Filosofiche"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/CRIHN.png"
    alt="Centre de recherche interuniversitaire sur les humanités numériques"
>}}

{{< /blocs/partenaires >}}


{{< blocs/prose id="contacts" >}}
## Contact Us

<a href="carla.canullo@unimc.it">carla.canullo@unimc.it</a>

<a href="tiberio.uricchio@unimc.it">tiberio.uricchio@unimc.it</a>

<a href="marcello.vitali.rosati@umontreal.ca">marcello.vitali.rosati@umontreal.ca</a>

The event will take place at the University of Macerata, aula Omero Proietti

Via Garibaldi 20, third floor.

{{< /blocs/prose >}}

