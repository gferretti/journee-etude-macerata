---
title: Guide de style
surtitre: Aperçu
layout: styleguide
---

Sem condimentum parturient hendrerit lacinia scelerisque sed a placerat quis semper taciti condimentum lectus in a a libero in amet elementum penatibus maecenas a lorem inceptos est senectus. Scelerisque ante vestibulum metus ante sem scelerisque ad dui ac class mus.

# En-tête de niveau 1

## En-tête de niveau 2

### En-tête de niveau 3

#### En-tête de niveau 4

##### En-tête de niveau 5

###### En-tête de niveau 6

Un paragraphe. La taille de base pour la prose est de `1.25rem` (soit `20px` avec les préférences par défaut des navigateurs).

Paragraphe avec du **gras** et de l'_italique_. Il pourrait y avoir du <mark>texte en surbrillance</mark>, quelques exposants<sup>x,y</sup> et même… du `code`. Voici une [ancre](https://www.umontreal.ca/) qui pointe vers un site externe.

> Une citation longue, automatiquement mise en retrait.
> 
> Deuxième paragraphe d'une citation longue.

Deux types de listes :

1. une
1. liste
1. numérotée

- une
- liste
  (blah)
  (bla)
- non ordonnée
  - et une autre
    - imbriquée

Une ligne horizontale.

---

{{< figure src="/images/partenaires/CRSH-color.png" caption="Une figure. Ici, le logo du CRSH." >}}
