---
title: Rejoindre en ligne
heroImage: /images/habitat-futuriste.jpg
---

### Lien pour la visioconférence

[**https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join**](https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join)

Ce lien est susceptible d'être modifié jusqu'au jour du colloque. Merci de revenir sur cette page et de l'actualiser le mardi 9 avril 2024.

l’événement se tiendra mardi 9 avril, de 9h00 à 12h20 et de 14h00 à 17h30 (UTC).

