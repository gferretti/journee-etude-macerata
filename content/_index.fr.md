---
title: "Human Beings and Machines:"
subtitle: "Stabilizing and Destabilizing Boundaries"
content: "9 avril 2024"
heroImage: /images/habitat-futuriste.jpg

# blocs:
# - type: prose
#   Content: |
#     Bla bla bla
---

{{< blocs/prose id="introduction" >}}

## Une journée d'étude consacrée aux relations entre humains et machines

Qu'est-ce qu'une machine&nbsp;? Qu'est-ce qu'un être humain&nbsp;? Existe-t-il une frontière claire et stable entre les deux&nbsp;? Les nombreux débats autour de l'IA ont récemment relancé l'intérêt pour ces questions qui nous hantent depuis des siècles. Il semblerait que dans notre relation avec les machines, dans la manière dont nous les définissons et les comprenons, et surtout dans la manière dont nous identifions en quoi elles sont différentes de nous, c'est notre essence même qui est en jeu. En tant qu'êtres humains, nous nous définissons par opposition aux machines.
Ce symposium vise à questionner cette ligne de démarcation en remettant en cause les oppositions entre humain et machine, artificiel et naturel, quantifiable et non-quantifiable, calculable et non-calculable.

{{< /blocs/prose >}}

{{< blocs/texte-exergue >}}
Università di Macerata/Université de Montréal
{{< /blocs/texte-exergue >}}

{{< blocs/horaire entete="Programme" id="programme" >}}
{{< blocs/item-horaire
    heure="9h00"
    titre="Mot de bienvenue"
    personnes="John Mc Court Recteur de l'Université de Macerata"
>}}
  
{{< blocs/item-horaire
    heure="9h15"
    titre="Introduction aux travaux: the day after... John McCarthy"
    personnes="Carla Canullo"
    resume=""
  >}}

{{< blocs/item-horaire
    heure="9h30"
    titre="L'accès aux ressources culturelles à l'ère de l'IA : concepts et défis"
    personnes="Pierluigi Feliciati"
    resume="Que savons-nous réellement de l'expérience d'accès et d'utilisation des ressources culturelles publiées sur Internet ? A partir de l'étude coordonnée dans le cadre du projet InterPARES Trust AI, l'intervenant présentera et discutera les principaux concepts et défis liés à la médiation et à l'accès. L'accent sera mis sur la qualité des ressources, en adoptant une approche centrée sur l'utilisateur pour la conception et le développement des ressources et en soulignant l'utilité des études d'utilisateurs pour concevoir, construire et maintenir des services numériques de qualité. Ensuite, le projet de modèle conceptuel d'accès/utilisation en cours de publication sera présenté et discuté, en soulignant les défis posés par le rôle des agents d'intelligence artificielle en tant qu'utilisateurs et réutilisateurs de contenu."
>}}

{{< blocs/item-horaire
    heure="10h10"
    titre="Réduire le gap sémantique dans la vision entre humains et machines"
    personnes="Tiberio Uricchio"
    resume=""
>}}

{{< blocs/item-horaire
    heure="10h50"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="11h00"
    titre="Human-AI Teaming à l'ère des systèmes intelligents"
    personnes="Emanuele Frontoni"
    resume="Cet exposé explore le concept de Human-AI Teaming, en mettant l'accent sur son impact transformateur sur les humanités numériques, la santé et le bien-être, et le développement durable. Dans le domaine des humanités numériques, les capacités avancées d'analyse de données de l'IA ont révolutionné la recherche historique, améliorant l'accès aux connaissances culturelles tout en remettant en question les méthodologies traditionnelles. Dans le domaine de la santé et du bien-être, l'IA permet de diagnostiquer avec précision et d'intervenir sur la santé mentale, ce qui améliore les résultats pour les patients et déstabilise les modèles de soins de santé conventionnels grâce à la médecine personnalisée. Les efforts en matière de développement durable bénéficient de l'IA grâce à l'amélioration de la surveillance de l'environnement et de la gestion de l'énergie, ce qui favorise une utilisation plus efficace des ressources et remet en question les pratiques environnementales existantes. L'exposé souligne la double capacité de l'équipe humain-IA à stabiliser et à déstabiliser les secteurs, et plaide en faveur de conceptions éthiques et centrées sur l'humain pour que ces intégrations contribuent positivement à la société."
>}}

{{< blocs/item-horaire
    heure="11h40"
    titre="Être en ligne et hors ligne : l'utopie de la présence naturelle"
    personnes="Peppe Cavallari"
    resume="La présence est toujours la présence de quelqu'un, ou de quelque chose, pour quelqu'un d'autre. L'analyse phénoménologique et l'archéologie des médias montrent que toute présence s'incarne et se matérialise comme l'effet d'une relation, d'une convention et d'un protocole technique, trois composantes de l'interface. L'expérience quotidienne des technologies de communication de la société numérique permet de démasquer un malentendu majeur : considérer la présence physique comme une forme de présence idéale, la seule présence réelle par opposition à la présence à distance médiatisée par les écrans, l'écriture et l'image. En nous opposant au mythe de la présence naturelle et immédiate, nous mettrons en évidence la structure inextricablement médiatisée et performative de la présence, en avançant l'hypothèse qu'à l'ère de la connexion permanente, la présence s'ouvre et se crée dans l'écart et le jeu spatio-temporel entre l'être en ligne et l'être hors ligne, deux phases de notre être-au-monde."
>}}

{{< blocs/item-horaire
    heure="12h20"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="14h00"
    titre="Le secrétaire et le patron sont d'accord : Réflexion sur les bonnes machines littéraires"
    personnes="Margot Mellet"
    resume="De la secrétaire à l'agent conversationnel, l'approche de l'écriture comme service mécanisable introduit des figures hybrides de femmes qui ne se limitent pas à une époque révolue. Qu'il s'agisse de l'imaginaire de la littérature, d'un phénomène culturel ou de réalités techniques, la femme est la figure qui sert la parole de l'homme : la petite main qui transcrit la pensée de l'homme sous sa dictée, ou l'intelligence vocale programmée pour répondre ou obéir à ses demandes. La mécanisation progressive de l'écriture moderne avec le développement des machines à écrire a produit un mouvement général de déplacement : la position de l'homme dans la chaîne de production change, et le genre de l'écriture s'inverse (Kittler, Gramophone, Film, Typewriter 1986), offrant aux femmes une carrière possible dans la chaîne d'édition. Cette porte d'entrée dans le monde de la littérature, où émergent les idées sur le monde, peut représenter une émancipation et une voie professionnelle à bien des égards, mais c'est aussi une arme à double tranchant. Dans le binôme secrétaire/patron/auteur, le partenariat est une affaire de service, de traitement de texte, mais aussi de domination d'un état d'être sur un autre. Dans ce cas, l'oscillation entre femme au service et machine à l'ordre fixe les limites d'une norme humaine essentiellement masculine. Jusqu'aux premiers agents conversationnels, largement inspirés d'une approche misogyne et hétéronormative du service, la machine véhicule un imaginaire d'un être masculin, un être qui engendre un idéal fortement transhumaniste du féminin. En analysant des moments littéraires et médiatiques -- les épouses écrivantes de plusieurs auteurs, dont Nietzsche, l'évolution culturelle de la machine à écrire, la figure de l'andreide dans l'Ève future de Villiers de L'Isle-Adam, les réactions misogynes des premiers agents conversationnels -- la présentation abordera le flou entre la machine et la femme qui est à la base de toute une culture littéraire, mais aussi d'une culture médiatique."
>}}

{{< blocs/item-horaire
    heure="14h40"
    titre="De la structure des données à la structure du monde"
    personnes="Giulia Ferretti"
    resume="Les structures de données organisent les connaissances en catégories distinctes et les rendent accessibles en ligne, contribuant ainsi à définir la réalité. Idéalement dépourvues d'ambiguïté, les architectures de données ont une valeur sémantique, de sorte qu'elles incarnent des informations cruciales pour la construction de nos opinions et de nos connaissances. Comment mettre en lumière l'influence des structures de données sur le sens des objets qu'elles représentent&nbsp;? Comment favoriser une réflexion collective sur leur influence sur la réalité qui nous entoure&nbsp;? En interrogeant diverses _Application Programming Interfaces_ (APIs), je montrerai comment les infrastructures techniques nous poussent et nous guident à redéfinir les objets décrits par ces technologies et, en même temps, j'étudierai les idées préconçues et les intérêts humains qui animent les structures de données examinées. Une telle enquête s'avérera nécessaire à la fois pour éviter le risque de nous résigner à l'incompréhensibilité des données et de leur influence et pour contrer toute croyance selon laquelle les structures organisant ces données ne sont que des conteneurs neutres d'idées et de comportements purement humains. En m'appuyant sur les conclusions théoriques du champ disciplinaire des études critiques du code, mon objectif est de démontrer que la structuration des données est un processus ontologiquement composé de pratiques discursives et culturelles et, en même temps, d'activités matérielles qui décomposent l'information, l'abstraient et, par conséquent, la conçoivent et la conceptualisent. Dans cet ensemble composite d'intérêts et de forces, les influences mécaniques et culturelles se redéfinissent mutuellement et participent à la signification des données structurées, déterminant leur influence sur notre réalité."
>}}

{{< blocs/item-horaire
    heure="15h20"
    titre="La chrysalide inhumaine"
    personnes="Ollivier Dyens"
    resume="Que signifie être humain dans un monde si étrange qu'il semble étranger, inhumain, non humain&nbsp;? Que signifie être humain dans un monde où les machines créent des œuvres d'art qui inspirent l'admiration, où les machines sont plus empathiques que nous, où les machines modifient notre façon de penser, d'aimer et de voir la beauté&nbsp;? Que signifie être humain dans un monde où la technologie est un cocon dont nous émergeons comme une chrysalide, mi-humaine, mi-«&nbsp;alienne&nbsp;»&nbsp;? Quelle sera notre nouvelle ontologie&nbsp;?"
>}}

{{< blocs/item-horaire
    heure="16h00"
    titre="Pause"
>}}

{{< blocs/item-horaire
    heure="16h10"
    titre="Interaction humain-machine et amour : La nouvelle génération de sites de rencontre"
    personnes="Michael Sinatra"
    resume="Les sites de rencontre ont toujours été fiers d'utiliser des méthodes mathématiques pour associer les personnes. Alors que la rhétorique de l'utilisation d'algorithmes «&nbsp;uniques&nbsp;» pour améliorer les associations continue à prendre de l'importance dans les publicités, elle a été suivie au cours des deux dernières années par des _prompts_ plus automatisés, pilotés par l'intelligence artificielle. Aujourd'hui, des échanges complets sont effectués par des outils assistés par l'I.A. tels que chatGPT, ce qui rend l'idée de rencontrer quelqu'un de «&nbsp;réel&nbsp;» encore plus complexe, même si c'est l'objectif premier de ces sites."
>}}

{{< blocs/item-horaire
    heure="16h50"
    titre="La fabrique des subalternes : Humains, machines et subordinations"
    personnes="Marcello Vitali-Rosati"
    resume="Depuis le lancement de ChatGPT, l'espace public est envahi par des discussions sur l'IA, la relation humain/machine et toute une série d'autres angoisses. Cet intérêt est révélateur de notre crainte effrénée que la place de «&nbsp;l'humain&nbsp;» dans le monde soit menacée. Ce qui nous angoisse dans les LLM, c'est qu'ils semblent mettre à mal les stratégies discursives qui permettent de créer des dispositifs hiérarchiques dans lesquels la séparation entre dominants et dominés est clairement définie. L'hypothèse que je vais présenter dans mon intervention est la suivante : les stratégies discursives de définition de l'humain par opposition à autre chose -- comme les animaux, les automates ou les machines -- sont en fait, toujours et avant tout, des stratégies de production de hiérarchies sociales, culturelles et, plus généralement, symboliques qui peuvent être comprises comme une véritable fabrique de subalternes."
>}}
{{< /blocs/horaire >}}


{{< blocs/partenaires entete="Nos partenaires" >}}

{{< blocs/item-partenaires
    src="/images/partenaires/unimc-logo-transparent.png"
    alt="Università di Macerata"
>}}


{{< blocs/item-partenaires
    src="https://horizons.ecrituresnumeriques.ca/images/Logo/UdeM.png"
    alt="Université de Montréal"
>}}

{{< blocs/item-partenaires
    src="/images/logo-fr.svg"
    alt="Chaire de recherche du Canada sur les écritures numériques"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/doppio-diploma-v1.png"
    alt="Doppio diploma in Scienze Filosofiche"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/CRIHN.png"
    alt="Centre de recherche interuniversitaire sur les humanités numériques"
>}}

{{< /blocs/partenaires >}}

{{< blocs/prose id="contacts" >}}

## Contacts

<a href="carla.canullo@unimc.it">carla.canullo@unimc.it</a>

<a href="tiberio.uricchio@unimc.it">tiberio.uricchio@unimc.it</a>

<a href="marcello.vitali.rosati@umontreal.ca">marcello.vitali.rosati@umontreal.ca</a>

L'événement se tiendra à l'Université de Macerata, salle de conférence Omero Proietti

Via Garibaldi 20, troisième étage.




{{< /blocs/prose >}}


