---
title: "Human Beings and Machines:"
subtitle: "Stabilizing and Destabilizing Boundaries"
content: "9 aprile 2024"
heroImage: /images/habitat-futuriste.jpg

# blocs:
# - type: prose
#   Content: |
#     Bla bla bla
---


{{< blocs/prose id="introduction" >}}

## Una giornata di studi consacrata alle relazioni tra umani e macchine

Che cos'è una macchina? Che cos'è un essere umano? Esiste un confine chiaro e stabile tra i due? Le numerose discussioni sull'IA hanno recentemente rinnovato l'interesse per queste domande, che ci perseguitano da secoli. Sembrerebbe che nel nostro rapporto con le macchine, nel modo in cui le definiamo e le comprendiamo, e soprattutto nel modo in cui identifichiamo le loro differenze rispetto a noi, sia in gioco la nostra stessa essenza. Come esseri umani, ci definiamo in opposizione alle macchine.
Questo simposio intende mettere in discussione tale linea di demarcazione, sfidando le opposizioni tra uomo e macchina, artificiale e naturale, quantificabile e non quantificabile, calcolabile e non calcolabile.

{{< /blocs/prose>}}


{{< blocs/texte-exergue >}}
Università di Macerata/Université de Montréal
{{< /blocs/texte-exergue >}}


{{< blocs/horaire entete="Programma" id="programma" >}}
{{< blocs/item-horaire
    heure="9:00"
    titre="Discorso di apertura"
    personnes="John Mc Court, Magnifico Rettore dell'Università di Macerata"
>}}
  
{{< blocs/item-horaire
    heure="9:15"
    titre="Introduzione ai lavori: The day after...  John McCarthy"
    personnes="Carla Canullo"
    resume=""
  >}}

{{< blocs/item-horaire
    heure="9:30"
    titre="L'accesso alle risorse culturali nell'era dell'IA: concetti e sfide"
    personnes="Pierluigi Feliciati"
    resume="Quanto sappiamo davvero dell'esperienza di accesso e fruizione delle risorse culturali pubblicate su Internet? Partendo dallo studio coordinato nell'ambito del progetto InterPARES Trust AI, il relatore presenterà e discuterà i principali concetti e sfide relativi alla mediazione e all'accesso. L'attenzione sarà focalizzata sulla qualità delle risorse, adottando un approccio centrato sull'utente nella concezione e nello sviluppo delle risorse ed evidenziando l'utilità degli studi sugli utenti per concepire, costruire e mantenere servizi digitali di qualità. Successivamente, verrà presentata e discussa la bozza del modello concettuale di accesso/uso in corso di rilascio, evidenziando le sfide aperte dal ruolo degli agenti di intelligenza artificiale come utenti e riutilizzatori dei contenuti."
>}}

{{< blocs/item-horaire
    heure="10:10"
    titre="Ridurre il gap semantico nella visione tra umani e macchine"
    personnes="Tiberio Uricchio"
    resume=""
>}}

{{< blocs/item-horaire
    heure="10:50"
    titre="Pausa"
>}}

{{< blocs/item-horaire
    heure="11:00"
    titre="Human-AI Teaming all'epoca dei sistemi intelligenti"
    personnes="Emanuele Frontoni"
    resume="Questo intervento esplora il concetto di Human-AI Teaming, sottolineando il suo impatto trasformativo nelle digital humanities, nella salute e nel benessere, e nella sostenibilità. Nelle digital humanities, le capacità avanzate di analisi dei dati dell'IA hanno rivoluzionato la ricerca storica, migliorando l'accessibilità alle conoscenze culturali e sfidando le metodologie tradizionali. Nel campo della salute e del benessere, l'IA aiuta a effettuare diagnosi precise e a intervenire sulla salute mentale, migliorando i risultati dei pazienti e destabilizzando i modelli sanitari convenzionali attraverso la medicina personalizzata. Gli sforzi per la sostenibilità traggono vantaggio dall'IA attraverso il miglioramento del monitoraggio ambientale e della gestione dell'energia, promuovendo un uso più efficiente delle risorse e sfidando le pratiche ambientali esistenti. L'intervento sottolinea la duplice capacità del team umano-intelligenza artificiale di stabilizzare e destabilizzare i settori, sostenendo la necessità di progetti etici e incentrati sull'umano per garantire che queste integrazioni contribuiscano positivamente alla società."
>}}

{{< blocs/item-horaire
    heure="11:40"
    titre="Essere on line e off line: utopia della presenza naturale"
    personnes="Peppe Cavallari"
    resume="La presenza è sempre presenza di qualcuno, o qualcosa, per qualcun altro. L'analisi fenomenologica e l'archeologia dei media dimostrano  che ogni presenza si incarna e si materializza come effetto di una relazione, di una convenzione e di un protocollo tecnico, tre componenti dell'interfaccia. L'esperienza quotidiana della tecnologia della comunicazione della società digitale permette di smascherare un equivoco madornale: considerare la presenza fisica come forma di presenza ideale, sola presenza reale rispetto alla presenza a distanza mediata dagli schermi, dalla scrittura e dall'immagine. Opponendoci al mito della presenza naturale e immediata, metteremo in evidenza la struttura inestricabilmente mediata e performativa della presenza, ipotizzando che all'epoca della connessione permamente la presenza si apre e si crea nello scarto e nel gioco spazio-temporale tra l'essere on-line e off-line, due fasi del nostro essere-nel-mondo."
>}}

{{< blocs/item-horaire
    heure="12:20"
    titre="Pausa"
>}}

{{< blocs/item-horaire
    heure="14:00"
    titre="Segretaria e capo sono d'accordo: Riflessione sulle buone macchine letterarie"
    personnes="Margot Mellet"
    resume="Da segretaria ad agente conversazionale, l'approccio alla scrittura come servizio meccanizzabile introduce figure ibride di donne che non si limitano a un'epoca passata. Che si tratti della forma immaginaria della letteratura, di un fenomeno culturale o di realtà tecniche, la donna è la figura che serve la parola dell'uomo: la _petite main_ che trascrive i pensieri dell'uomo sotto dettatura, o l'intelligenza vocale programmata per rispondere o obbedire alle sue richieste. La progressiva meccanizzazione della scrittura moderna con lo sviluppo delle macchine da scrivere ha prodotto un generale spostamento di equilibri: cambia la posizione dell'uomo nella filiera produttiva e si inverte il genere della scrittura (Kittler, Grammofono, film, macchina da scrivere 1986). Si presenta così alle donne una possibile carriera nella filiera editoriale. Questa porta di accesso al mondo della letteratura, dove emergono le idee del mondo, può rappresentare per molti aspetti una forma di emancipazione, ma è anche un'arma a doppio taglio. Nel binomio segretaria/capo/autore, la collaborazione è una questione di servizio, di elaborazione di testi, ma anche di dominio di uno stato d'essere su un altro. In questo caso, l'oscillazione tra donna al servizio e macchina in ordine stabilisce i confini di una norma umana essenzialmente maschile. Fin dai primi agenti conversazionali, che sono in gran parte ispirati a un approccio misogino ed eteronormativo alla nozione di servizio, la macchina trasmette un immaginario di un essere maschile, un essere che genera un ideale altamente transumanista del femminile. Analizzando momenti letterari e mediatici -- le coniugi scrittrici di diversi autori, tra cui Nietzsche, l'evoluzione culturale della macchina da scrivere, la figura dell'andreide nell'Eva futura di Villiers de L'Isle-Adam, le reazioni misogine dei primi agenti conversazionali -- la presentazione discuterà la sfocatura tra macchina e donna che è alla base di un'intera cultura letteraria, ma anche di una cultura mediatica."
>}}

{{< blocs/item-horaire
    heure="14:40"
    titre="Dalle strutture di dati alla struttura del mondo"
    personnes="Giulia Ferretti"
    resume="Le strutture di dati organizzano la conoscenza in categorie distinte e la rendono accessibile online, contribuendo a definire il reale. Idealmente prive di ambiguità, le architetture di dati hanno valore semantico, tanto da incarnare informazioni cruciali per la costruzione di opinioni e conoscenze. Come possiamo mettere in luce l'influenza delle strutture di dati sul significato degli oggetti che rappresentano? Come incoraggiare una riflessione collettiva sulla loro influenza sulla realtà che ci circonda? Attraverso l'interrogazione di diverse _Application Programming Interfaces_ (APIs), mostrerò che le infrastrutture tecniche ci sfidano e ci guidano a ridefinire gli oggetti descritti attraverso di esse e, allo stesso tempo, indagherò i preconcetti e gli interessi umani che determinano le strutture di dati prese in analisi. Tale indagine si rivelerà necessaria sia per ovviare al rischio di rassegnarsi all'incomprensibilità dei dati e della loro influenza, sia per contrastare la convinzione che le strutture che organizzano tali dati siano solo contenitori neutri di idee e comportamenti puramente umani. A partire dalle conclusioni teoriche del campo disciplinare dei critical code studies, dimostrerò che la strutturazione di dati è un processo ontologicamente composto da pratiche discorsive e culturali e, allo stesso tempo, da attività materiali che decompongono le informazioni, le astraggono e quindi le progettano e le concettualizzano. In questo insieme composito di interessi e forze, le influenze macchiniche e culturali si ridefiniscono a vicenda e partecipano alla significazione dei dati strutturati, determinando la loro influenza sulla nostra realtà."
>}}

{{< blocs/item-horaire
    heure="15:20"
    titre="La crisalide disumana"
    personnes="Ollivier Dyens"
    resume="Cosa significa essere umani in un mondo così strano da sembrare alieno, inumano, non umano? Cosa significa essere umani in un mondo in cui le macchine creano opere d'arte che incantano, in cui le macchine sono più empatiche di noi, in cui le macchine modificano il nostro modo di pensare, di amare e di vedere la bellezza? Cosa significa essere umani in un mondo in cui la tecnologia è un bozzolo da cui emergiamo come una crisalide, metà umani e metà \"alieni\"? Quale sarà la nostra nuova ontologia?"
>}}

{{< blocs/item-horaire
    heure="16:00"
    titre="Pausa"
>}}

{{< blocs/item-horaire
    heure="16:10"
    titre="Interazione umano-macchina e amore: La nuova generazione di siti di incontri"
    personnes="Michael Sinatra"
    resume="I siti di incontri hanno sempre vantato l'utilizzo di metodi matematici per abbinare le persone. Mentre la retorica dell'uso di algoritmi \"unici\" per migliorare gli abbinamenti continua a crescere nelle pubblicità, negli ultimi due anni tale retorica è affiancata da _prompt_ più automatizzati, guidati dall'intelligenza artificiale. Ora scambi completi sono effettuati da strumenti assistiti dall'IA come chatGPT, rendendo l'idea di incontrare qualcuno di \"reale\" ancora più complessa, per quanto sia lo scopo primario di questi siti."
>}}

{{< blocs/item-horaire
    heure="16:50"
    titre="La fabbrica dei subalterni: Umani, macchine e subordinazioni"
    personnes="Marcello Vitali-Rosati"
    resume="Sin dal lancio di ChatGPT, la scena pubblica si è riempita di discorsi sull'IA, sul rapporto umano/macchina e su una serie di altre ansie. Questo interesse rivela la nostra paura sfrenata che il posto dell'\"umano\" nel mondo sia minacciato. Ciò che ci angoscia delle LLM è il fatto che sembrano minare le strategie discorsive che permettono di creare dispositivi gerarchici in cui la separazione tra dominanti e dominati è chiaramente definita. L'ipotesi che presenterò nel mio intervento è la seguente: le strategie discorsive per definire l'umano in opposizione ad altro -- animali, automi o macchine -- sono in realtà, sempre e soprattutto, strategie per produrre gerarchie sociali, culturali e, più in generale, simboliche che possono essere intese come una vera e propria fabbrica di subalterni."
>}}

{{< /blocs/horaire >}}


{{< blocs/partenaires entete="I nostri partner" id="partners" >}}

{{< blocs/item-partenaires
    src="/images/partenaires/unimc-logo-transparent.png"
    alt="Università di Macerata"
>}}


{{< blocs/item-partenaires
    src="https://horizons.ecrituresnumeriques.ca/images/Logo/UdeM.png"
    alt="Université de Montréal"
>}}

{{< blocs/item-partenaires
    src="/images/logo-fr.svg"
    alt="Chaire de recherche du Canada sur les écritures numériques"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/doppio-diploma-v1.png"
    alt="Doppio diploma in Scienze Filosofiche"
>}}

{{< blocs/item-partenaires
    src="/images/partenaires/CRIHN.png"
    alt="Centre de recherche interuniversitaire sur les humanités numériques"
>}}


{{< /blocs/partenaires >}}


{{< blocs/prose id="contatti" >}}

## Contatti

<a href="carla.canullo@unimc.it">carla.canullo@unimc.it</a>

<a href="tiberio.uricchio@unimc.it">tiberio.uricchio@unimc.it</a>

<a href="marcello.vitali.rosati@umontreal.ca">marcello.vitali.rosati@umontreal.ca</a>

L'evento si terrà presso l'Università di Macerata, aula Omero Proietti

Via Garibaldi 20, terzo piano.


{{< /blocs/prose >}}

