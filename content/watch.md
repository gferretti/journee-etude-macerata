---
title: Join online
heroImage: /images/habitat-futuriste.jpg
---

### Link to the conference

[**https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join**](https://bbb.futuretic.fr/rooms/ukp-g4y-ukn-1jc/join)

This link is subject to change until the symposium day. Please come back to this page and update it on April 9, 2024.

The event will be held on Tuesday, April 9, from 9:00 a.m. to 12:20 p.m. and from 2:00 p.m. to 5:30 p.m. (UTC).


