# Note pour le développement du thème

Les fichiers HTML et CSS sont élaborés selon une méthodologie modulaire, inspirée du design par composantes (_component-based design_; _Single-File Components_ (SFC)) exemplifié dans des cadres de travail plus _high-tech_ (Svelte, Vue.js, etc.).

## Styles

Les feuilles de style sont écrites séparément dans le dossier `assets/styles/` :

1. `assets/styles/general/` :
   - `reset.css` - uniformiser les styles entre les navigateurs.
   - `variables.css` - variables CSS utilisées partout dans le design (couleurs, typographie, etc.)
   - `global.css` - styles globaux, à utiliser avec beaucoup de parcimonie, et surtout le monis possible.
2. `assets/styles/fonts/` : fichiers de police (une feuille par famille de police utilisée)
3. `assets/styles/layouts/` : un fichier pour chaque disposition correspondante (`layouts/`).
   - `home-layout.css` => `layouts/home.html` 
   - `page-layout.css` => `layouts/single.html`
4. `assets/styles/components` : un fichier pour chaque composant correspondant (`layouts/partials/`), lorsqu'une feuille de style est nécessaire.

En développement, les feuilles seront chargées séparément pour faciliter le déboggage.
Lors de la construction du site pour publication, Hugo fera une concaténation de toutes les feuilles de style en une seule.

### Ajout d'une feuille de style

Les feuilles de style sont renseignées dans `layouts/partials/styles.head.html`.
Chaque nouvelle feuille devra être ajoutée dans ce fichier pour être incluse dans le site.

## Auteur

Louis-Olivier Brassard <louis-olivier.brassard@umontreal.ca>
