# Microsite pour une journée d'étude

Petit site web, pouvant comporter une seule page ou plusieurs, pour afficher le programme d'une journée d'étude dans le milieu universitaire.
Disponible en autant de langues qu'on le veut ! (Voir la documentation sur cette page pour le [multilinguisme](#multilinguisme).)

## Prérequis

- [Hugo](https://gohugo.io/) `v0.123+` est requis.
- Un éditeur de texte pour rédiger des fichiers Markdown, TOML, Yaml, etc.

## Rédaction

La rédaction se fait dans des fichiers markdown dans le dossier `content/`.

- Page d'accueil : `content/_index.md` => `/`
- Page régulière : `content/page.md` => `/page/`

### Dispositions disponibles

- **Page d'accueil** (`layout: home`) : la page d'accueil utilise par défaut une disposition par **blocs**, avec une section « hero » incluse par défaut.
- **Page blocs** (`layout: blocs`) : le corps doit être rédigé **avec les blocs** (voir la syntaxe plus bas).
- **Page régulière** (`layout: single`) : le corps est rédigé en prose régulière.

### Blocs
La disposition sous forme de « blocs », des composants modulaires, permet une rédaction enrichie d'éléments modulaires et non confinés à l'intérieur d'un conteneur fixe.
Cela se fait grâce à la syntaxe des [shortcodes](https://gohugo.io/content-management/shortcodes/) Hugo.
On écrira directement les shortcodes dans le corps du fichier Markdown (voir les notes d'usage pour les blocs suivants).

#### `blocs/hero`
Grande section avec un titre en gros et une image de fond.

<details>
<summary><strong>Aperçu</strong></summary>

![Aperçu du bloc](docs/bloc-hero.png)

</details>

- `titre` : titre, à afficher en gros
- `soustitre` : sous-titre, en texte plus petit
- `image` : chemin de l'image de fond.

Usage :

```markdown
{{< blocs/hero
    titre="Titre de la section"
    soustitre="Sous-titre en plus petit"
    image="/chemin/vers/image.ext"
/>}}
```

#### `blocs/horaire`
Conteneur d'une section horaire.

<details>
<summary><strong>Aperçu</strong></summary>

![Aperçu du bloc](docs/bloc-horaire.png)

</details>

- `entete` : en-tête pour bloc (optionnel).
- `id` : identifiant sur l'élément HTML (optionnel).

Usage :

```markdown
{{< blocs/horaire entete="En-tête pour bloc" id="id-du-bloc" >}}

{{< blocs/item-horaire heure="09:00" personnes="M. X, Mme Y" resume="Résumé ..." titre="Titre ..." >}}

{{< /blocs/horaire >}}
```

#### `blocs/item-horaire`
Enfant de `blocs/horaire`.

- `heure` : heure, saisie en texte libre
- `personnes` : nom de personnes, séparées par des virgules
- `résumé` : texte de description plus long.
- `titre` : titre, en gros carctères

#### `blocs/partenaires`
Grille de logos 3x3 avec un espacement harmonieux.

<details>
<summary><strong>Aperçu</strong></summary>

![Aperçu du bloc](docs/bloc-partenaires.png)

</details>

- `entete` : en-tête pour bloc (optionnel).
- `id` : identifiant sur l'élément HTML (optionnel).

Usage :

```markdown
{{< blocs/partenaires entete="En-tête pour bloc" id="id-du-bloc" >}}

{{< blocs/item-partenaires src="chemin/vers/image.ext" alt="Texte alternatif" >}}

{{< /blocs/partenaires >}}
```

#### `blocs/item-partenaires`
Enfant de `blocs/partenaires`.

- `src` : chemin vers l'image (logo du partenaire).
- `alt` : texte alternatif de l'image (bonne pratique pour l'accessibilité aux malvoyants, ou s'il y a un problème de chargement de l'image)

#### `blocs/prose`
Prose régulière, comme si on rédigeait directement dans le corps d'un fichier Markdown d'une disposition ordinaire.

<details>
<summary><strong>Aperçu</strong></summary>

![Aperçu du bloc](docs/bloc-prose.png)

</details>

Usage :

```markdown
{{< blocs/prose id="id-du-bloc" >}}
## Mon contenu markdown

Bla _bla_ bla.
{{< /blocs/prose >}}
```

#### `blocs/texte-exergue`
Section large, avec du texte en gros.

<details>
<summary>Aperçu</summary>

![Aperçu du bloc](docs/bloc-exergue.png)

</details>

```markdown
{{< blocs/texte-exergue >}}
Mon contenu **mis en avant** (avec markdown ou HTML).
{{< /blocs/texte-exergue >}}
```

## Multilinguisme
Ce site peut être décliné en plusieurs langues, grâce aux [fonctionnalités multilingues](https://gohugo.io/content-management/multilingual/) de Hugo.

Pour ce faire, il faut d'abord configurer les langues dans le fichier `config/_default/languages.toml`.
On peut y définir l'ordre d'apparition des langues avec la propriété `weight`.

```toml
[fr]
  weight = 1
  languageName = "Français"
#  title = ""
  [fr.params]
    # description = ""
    # images = [  ]
    # copyright = ""
    # location = ""

# ... et ainsi de suite pour chaque langue
```

Les paramètres par défaut (dans le bloc `[lang.params]`) sont spécifiés dans le fichier de configuration principal `config.toml`.

### Pages multilingues
La rédaction des contenus se fait dans des fichiers **suffixés par le code de la langue**.
Dans Hugo, une page correspond au fichier `content/page.<lang>.md`, où `<lang>` correspond au code de langue (ex. `fr`, `en`, etc.).
Le code de langue sera ajouté dans le chemin (URL) des pages, ex. `/en/page-en-anglais/`, sauf pour langue par défaut (`/page/`).

### Menus multilingues
Pour modifier les entrées de menu, on modifiera le fichier TOML correspondant `config/_default/menus.<lang>.toml`, où `<lang>` correspond au code de langue (ex. `fr`, `en`, etc.).

## Prévisualisation

Dans un terminal et à la racine du projet, on aura recours à la commande hugo pour lancer un serveur local, par défaut à l'adresse `http://localhost:1313/` :

```shell
hugo serve
```

## Construction

En lançant la commande `hugo`, le site sera construit dans le dossier `public/` à la racine de ce projet :

```shell
hugo
```

Note : le dossier `public/` n'est pas suivi par Git.

## Hébergement

Prêt pour déploiement en continu avec GitHub Pages (fichier de configuration : `.github/workflows/pages.yml`) ou GitLab Pages (fichier de configuration : `.gitlab-ci.yml`).

## Développement

Voir le fichier [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Licence

[WTFPL](http://www.wtfpl.net/) 2024 Chaire de recherche du Canada sur les écritures numériques.
